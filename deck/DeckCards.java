package deck;

import card.Card;

import java.util.ArrayList;
import java.util.Collections;

public class DeckCards {
    private ArrayList<Card> cards;

    public DeckCards() {
        this.cards = new ArrayList<Card>();
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }

    public Card draw() {
        if (this.cards.size() > 0) {
            return this.cards.remove(0);
        }
        return null;
    }

    public int getSize() {
        return this.cards.size();
    }
}

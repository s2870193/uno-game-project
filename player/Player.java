package player;

import card.Card;

import java.util.ArrayList;

public class Player {
    private String name;
    private int score;

    private ArrayList<Card> cards;

    public Player (String name, int score, ArrayList<Card> cards) {
        this.name = name;
        this.score = score;
        this.cards = cards;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public int getScore () {
        return score;
    }

    public void setScore (int score) {
        this.score = score;
    }

    public ArrayList<Card> getCards () {
        return cards;
    }

    public void setCards (ArrayList<Card> cards) {
        this.cards = cards;
    }
}

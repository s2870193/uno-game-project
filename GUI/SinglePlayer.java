package GUI;

/*
Java packages
 */
import javax.swing.*;
import java.io.IOException;
import java.io.PrintStream;

public class SinglePlayer extends JFrame {
    private JTextField usernameField;
    private JButton usernameAccept;
    private JPanel singlePlayer;
    private JTextArea consoleArea;
    private JTextField consoleCommandSP;
    private JButton sendCommandSP;

    private PrintStream standardOut;

    public SinglePlayer () {
        setContentPane (singlePlayer);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        setResizable (false);
        setTitle ("-Java® Uno Game-");
        setSize (600, 420);
        consoleArea.setEditable (false);

        ImageIcon mainIcon = new ImageIcon ("logo.png");
        setIconImage (mainIcon.getImage ());


        /*
        JTextArea to cosole display.
         */

        PrintStream printStream = new PrintStream (new CustomOutputStream (consoleArea));

        standardOut = System.out;

        System.setOut (printStream);
        System.setErr (printStream);

        setVisible (true);
    }
    public void printConsole() {
        Thread thread = new Thread (new Runnable () {
            @Override
            public void run () {
                while(true) {
                    System.out.println ("Hello");
                    try {
                        Thread.sleep (1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace ();
                    }
                }
            }
        });
        thread.start ();
    }

    public static void main (String[] args) {
        SwingUtilities.invokeLater (new Runnable () {
            @Override
            public void run () {
                new SinglePlayer ().setVisible (true);
            }
        });
    }
}

package GUI;

import javax.swing.*;

public class MultiPlayer extends JFrame {
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JTextField userNameMultiPlayer;
    private JButton userNameMultiPlayerAccept;
    private JTextField consoleCommandsMP;
    private JButton sendCommandMP;
    private JPanel multiPlayer;

    public MultiPlayer () {

        setContentPane (multiPlayer);

        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

        setResizable (false);

        setTitle ("-Java® Uno Game-");

        setSize (700, 560);

        ImageIcon mainIcon = new ImageIcon ("logo.png");

        setIconImage (mainIcon.getImage ());

        setVisible (true);
    }
}
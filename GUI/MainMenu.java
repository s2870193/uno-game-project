package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Set;

public class MainMenu extends JFrame{
    private JPanel panel1;
    private JButton singlePlayerButton;
    private JButton multiplayerButton;
    private JButton settingsButton;
    private JButton exitButton;
    private JLabel welcome;
    private JLabel backgroundImage;

    /*
    public static class BackgroundImage extends JComponent {
        private final Image image;
        public BackgroundImage(Image image) {
            this.image = image;
        }
        @Override
        protected void paintComponent(Graphics gp) {
            super.paintComponent (gp);
            gp.drawImage(image, 0,0,this);
        }
    }
     */

    public MainMenu() {
        setContentPane (panel1);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        setResizable (false);
        setTitle ("-Java® Uno Game-");
        setSize (420, 620);

        ImageIcon mainIcon = new ImageIcon ("logo.png");
        setIconImage (mainIcon.getImage ());

        backgroundImage.setVisible (true);
        backgroundImage.setIcon (new ImageIcon ("GUI/background_menu.jpg"));

        setVisible (true);

        /*
        Handling background image

        BufferedImage background = ImageIO.read (new File ("GUI/background_menu.jpg"));
        setContentPane (new BackgroundImage (background));
         */


        singlePlayerButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                setVisible (false);
                SinglePlayer SP = new SinglePlayer ();
            }
        });
        multiplayerButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                setVisible (false);
                MultiPlayer MP = new MultiPlayer ();
            }
        });
        settingsButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                setVisible (false);
                Settings ST = new Settings ();
            }
        });
        exitButton.addActionListener (new ActionListener () {
            @Override
            public void actionPerformed (ActionEvent e) {
                dispose ();
            }
        });
    }



    public static void main (String[] args) {
        try {
            UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
        } catch (Exception ex) {
            ex.printStackTrace ();
        }
        Runnable r = new Runnable () {
            @Override
            public void run () {
               new MainMenu ();
            }
        };
        EventQueue.invokeLater (r);
    }
}
/*
package test;

import static org.junit.jupiter.api.Assertions.*;

import card.Card;
import deck.DeckCards;
import org.junit.jupiter.api.Test;
import player.Player;

public class UNOTest{
    @Test
    public void testCard() {
        Card redThree = new Card("RED", 3);
        assertEquals(Card.Colour.RED, redThree.getColour());
        assertEquals(Card.Value.THREE, redThree.getValue());
        assertEquals("RED THREE", redThree.toString());
    }

    @Test
    public void testDeck() {
        // Create a new deck and add some cards to it
        DeckCards deck = new DeckCards();
        deck.addCard(new Card("Red", 1));
        deck.addCard(new Card("Yellow", 2));
        deck.addCard(new Card("Green", 3));
        deck.addCard(new Card("Blue", 4));

        // Test the size of the deck
        assertEquals(4, deck.getSize());

        // Test drawing a card
        Card card = deck.draw();
        assertEquals("Red", card.getColour());
        assertEquals(1, card.getValue());

        // Test shuffling the deck
        deck.shuffle();

        // Test drawing all the remaining cards
        assertEquals(3, deck.getSize());
        card = deck.draw();
        card = deck.draw();
        card = deck.draw();
        assertEquals(0, deck.getSize());

        // Test drawing from an empty deck
        card = deck.draw();
        assertNull(card);
    }

    @Test
    public void testPlayer() {
        Player player = new Player("Alice","Johnson");
        assertEquals("Alice", player.getName());
        assertEquals("Johnson", player.getSurname());
        assertEquals("Alice Johnson", player.toString());
    }
}
 */

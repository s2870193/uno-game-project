package game;

public interface Game {
    boolean running();
    boolean gameIsOver();

    int players();

    String gameMode();


}

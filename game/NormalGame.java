
package game;

import java.util.*;

public class NormalGame implements Game{
    @Override
    public boolean running () {
        return true;
    }

    public boolean gameIsOver() {
         return false;
    }

    @Override
    public int players () {
        return 0;
    }

    @Override
    public String gameMode () {
        return null;
    }

    /*
    S - Skip card
    R - Reverse card
    D - Draw two
    W - Wild card
    F - Wild card + 4
     */

    public NormalGame () {
        while(!gameIsOver ()) {
            System.out.println ("Hello");
        }
    }
}